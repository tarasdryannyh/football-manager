<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Player;
use yii\web\NotFoundHttpException;
use yii\helpers\Html;
use yii\grid\ActionColumn;

class PlayerController extends Controller
{

    public $layout = 'main.twig';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex($team_id)
    {
       return $this->renderTable($team_id);
    }


    public function actionCreate()
    {
        $model = new Player();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['player/index', 'team_id' => Yii::$app->request->get('team_id')]);
        } else {
            return $this->render('create.twig', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id,$team_id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'team_id' => Yii::$app->request->get('team_id')]);
        } else {
            return $this->render('update.twig', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id,$team_id)
    {
        $this->findModel($id)->delete();
            return $this->renderTable($team_id);
    }

    protected function findModel($id)
    {
        if (($model = Player::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function renderTable($team_id)
    {
        $action_column = $this->actionColumn();
        $searchModel = new Player();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$team_id);
        $method = Yii::$app->request->isAjax? 'renderAjax' : 'render';
        if($dataProvider!=null){
        return $this->$method('index.twig', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'action_column' => $action_column,
        ]);
        }
        else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionColumn(){
        $action_column = [
            'class' => ActionColumn::className(),
                'template' => '{delete} {update}',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['/player/delete', 'team_id' => Yii::$app->request->get('team_id'), 'id' => $model->id], [
                                'title' => Yii::t('yii', 'Delete'),
                                'data-pjax' => '#model-grid',
                                'data-method' => 'post',
                                'data-confirm' => 'Are you sure you want to delete?',
                            ]);
                        },
                    'update' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['/player/update', 'team_id' => Yii::$app->request->get('team_id'), 'id' => $model->id], [
                                'title' => Yii::t('yii', 'Update'),
                            ]);
                        },
                ]
        ,];
        return $action_column;
    }
}
