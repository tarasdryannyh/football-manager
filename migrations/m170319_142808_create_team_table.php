<?php

use yii\db\Migration;

/**
 * Handles the creation of table `team`.
 */
class m170319_142808_create_team_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('team', [
            'id'  => $this->primaryKey(),
            'name'=> $this->string(255)->notNull(),
            'year'=> $this->integer(11)->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('team');
    }
}
