<?php

use yii\db\Migration;

/**
 * Handles the creation of table `players`.
 */
class m170320_055446_create_players_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('players', [
            'id'      => $this->primaryKey(),
            'team_id' => $this->integer(),
            'name'    => $this->string(255)->notNull(),
            'surname' => $this->string(255)->notNull(),
            'year'    => $this->integer(11)->notNull(),
            'position'=> $this->string(255)->notNull(),
        ]);

        $this->addForeignKey(
            'fk-team_id',
            'players',
            'team_id',
            'team',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('players');
    }
}
