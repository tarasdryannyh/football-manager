<?php

namespace app\models;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

class Player extends ActiveRecord{

    /**
     * @inheritdoc
     */

    public static function tableName(){
        return 'players';
    }

    public function rules()
    {
        return [
            [['name','surname','position','year','team_id'],'required'],
            [['name','surname','position'],'string'],
            [['year'], 'date', 'format' => 'php:Y-m-d'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID гравця',
            'name' => 'Імя',
            'surname' => 'Прізвище',
            'position' => 'Позиція на полі',
            'year' => 'Дата народження (Y-m-d)',
        ];
    }

    public function search($params,$id)
    {
        $query = Player::find()->where('team_id='.$id)->indexBy('id')->orderBy(['id' => SORT_ASC]);
        if($query===null)
            return false;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }

}