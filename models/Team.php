<?php

namespace app\models;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;

class Team extends ActiveRecord{
    public $bb;

//    function __construct()
//    {
//        parent::__construct();
//        $this->bb =  $this->id;
//    }

    public static function tableName(){
        return 'team';
    }

    public function rules()
    {
        return [
            [['name','year'],'required'],
            [['name'], 'string'],
            [['year'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID команди',
            'name' => 'Назва',
            'year' => 'Рік заснування',
        ];
    }

    public function search($params)
    {
        $query = Team::find()->indexBy('id')->orderBy(['id' => SORT_ASC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,

        ]);

        $this->load($params);

        $query->andFilterWhere([
            'id' => $this->id,
            'name' => $this->name,
            'year' => $this->year,
        ]);

        return $dataProvider;
    }

    public function getLink(){
        return '1231231';
    }

}